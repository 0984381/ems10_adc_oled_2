#include <msp430.h> 
#include <stdint.h>

/* Functieprototypen voor de meegeleverde "oled_lib" */

/* Deze functie configureert het display
 * via i2c en tekent alvast het kader.
 */
void initDisplay();

/* Deze functie past de weer te geven
 * temperatuur aan.
 *
 * int temp: De weer te geven temperatuur.
 * Max is 999, min is -99.
 * Hierbuiten wordt "Err-" weergegeven.
 *
 * Voorbeeld:
 * setTemp(100); // geef 10.0 graden weer
 */
void setTemp(int temp);

/* Deze functie geeft bovenaan in het
 * display een titel weer.
 * char tekst[]: de weer te geven tekst
 *
 * Voorbeeld:
 * setTitle("Hallo"); // geef Hallo weer
 */
void setTitle(char tekst[]);

int temp_celsius(int ADC_waarde)
{
    int tiendegraden = (ADC_waarde / 1023.0) * 150;
    return tiendegraden;
}

#pragma vector = ADC10_VECTOR
__interrupt void mijnADCInterruptRoutine(void)
{
    // deze code wordt uitgevoerd bij een ADC interrupt
    static int temperatuur = 10;
    temperatuur = temp_celsius(ADC10MEM);
    setTemp(temperatuur);
}

#pragma vector = TIMER0_A0_VECTOR

__interrupt void Timer_A(void)
{
    ADC10CTL0 |= ADC10SC;
}

int main(void)
{
	WDTCTL = WDTPW | WDTHOLD; // Stop watchdog timer
	
	// Stel klok in op 16MHz
    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ;
    DCOCTL = CALDCO_16MHZ;

    ADC10CTL1 = INCH_5 | ADC10DIV_7;
    ADC10CTL0 = SREF_1 | ADC10SHT_3 | ADC10SR | REFBURST | REFON | ADC10ON | ADC10IE | ENC;
    TA0CTL = TASSEL_1 | MC_1;    // instellingen van de timer A0
    TACCR0 = 50000;                        // stel capture control register 0 in
    TA0CCTL0 = CCIE;         // Stel de interrupt in
    BCSCTL3 = 0b00100000;               // VLOCLK
    _BIS_SR(GIE);                       // Global activation of interrupts
    __enable_interrupt();

	initDisplay();
	setTitle("Opdracht7.1.13");

	//uint8_t temperatuur = 0; // Fictieve temperatuur

	while(1)
	{
	    // Hoog fictieve temperatuur op met 0.1 graad
	    // Geef dit weer op het display
	    //setTemp(temperatuur--);
	    // Niet te snel...
	    //__delay_cycles(4000000);
//        ADC10CTL0 |= ADC10SC;
//        __delay_cycles(16000000);
        __low_power_mode_3();
	}
}
